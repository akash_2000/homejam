const mongoose = require('mongoose');

const classInstructionSchema = mongoose.Schema({
    instruction_title:{
        type: String
    }
})

const courseStudentSchema = mongoose.Schema({
    student_id : {
        type: String
    },
    student_name: {
        type: String
    },
    student_email:{
        type: String,
        unique: false
    }
})

const teacherSchema = mongoose.Schema({
    teacher_name: {
        type: String
    },
    teacher_email: {
        type: String
    }
})

const classesSchema = mongoose.Schema({
    class_name: {
        type: String,
        required: true
    },
    class_instructions: [classInstructionSchema],
    teacher_info: teacherSchema,
    students : [courseStudentSchema]
}, {collection: 'classes'})
const model = mongoose.model('classes', classesSchema);
module.exports = model;