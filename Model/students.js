const mongoose = require('mongoose');

const classInstructionSchema = mongoose.Schema({
    instruction_title : {
        type: String
    }
})

const classesSchema = mongoose.Schema({
    class_name : {
        type: String
    },
    class_instructions: [classInstructionSchema],
    class_teacher: {
        type: String
    }
})

const studentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String
    },
    classes: [classesSchema]
},{collection: 'students'});
const model = mongoose.model('students', studentSchema);
module.exports = model;