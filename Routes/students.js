const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const students = require('../Model/students');
const classes = require('../Model/classes');

router.post('/register', async (req, res) => {
    const {
        name,
        email,
        password:plaintext, 
        classes
    } = req.body;
    const role = "student";
    if(!name || typeof name != 'string'){
        return res.json({status: 'error', msg: 'Invalid Name'})
    }
    if(!plaintext || typeof plaintext != 'string'){
        return res.json({status: 'error', msg: 'Invalid password'})
    }
    if(plaintext.length < 6){
        return res.json({status: 'error', msg: 'Password should greater than 6 character'})
    }
    const password = await bcrypt.hash(plaintext, 10);
    try{
        const newStudent = await students.create({
            name,
            email,
            password, 
            role,
            classes
        })
        res.send("Registration successfully")
    } catch(err){
        throw err;
    }
    console.log(req.body)
})

router.post('/sign_in', async (req, res) => {
    const {email, password} = req.body;
    const getStudent = await students.findOne({email}).lean();
    if(!getStudent){
        res.json({status: 'ok', msg: 'Email not found!!!'})
    }
    if(await bcrypt.compare(password, getStudent.password)){
        return res.json({status: 'ok', msg: 'Student login successfull'})
    }
    else{
        return res.json({status: 'ok', msg: 'Incorrect email or password!!!'})
    }
})

// enroll for a class
router.get('/:students_id/:class_id/enroll_class', async (req, res) => {
    const {students_id, class_id} = req.params;

    const getClass = await classes.find({'_id': class_id})
    const getStudent = await students.find({'_id': students_id}) 

    const {_id, class_name, class_instructions} = getClass[0];
    const {_id:student_id, name:student_name, email:student_email} = getStudent[0]
    const newClass = {
        class_name,
        class_instructions
    }

    const newStudent = {
        student_id,
        student_name,
        student_email
    }

    try{
        const addClass = await students.updateOne(
            {'_id': students_id},
            {
                "$push":
                {
                    "classes": newClass
                }
            }
        )
        const addStudent = await classes.updateOne(
            {'_id': class_id},
            {
                "$push":
                {
                    "students": newStudent
                }
            }
        )
        res.send("Course enrolled successfully!!!")
    }
    catch(err){
        throw err;
    }
})

// classes list
router.get('/:students_id/classes_list', async (req, res) => {
    const {students_id} = req.params;
    const getStudent = await students.find({'_id': students_id}).lean() 
    console.log(getStudent)
    const classesList = getStudent[0].classes;
    res.send(classesList) 
})

module.exports = router;