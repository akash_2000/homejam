const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const teachers = require('../Model/teachers');
const classes = require('../Model/classes');


const JWT_SECRET = "kakhshj!@#%$#&*hjfjhaj659uhdsuuhsdjkehdhu";

router.post('/register', async (req, res) => {
    const {
        name,
        email,
        password:plaintext, 
    } = req.body;
    const role = "teacher";
    if(!name || typeof name != 'string'){
        return res.json({status: 'error', msg: 'Invalid Name'})
    }
    if(!plaintext || typeof plaintext != 'string'){
        return res.json({status: 'error', msg: 'Invalid password'})
    }
    if(plaintext.length < 6){
        return res.json({status: 'error', msg: 'Password should greater than 6 character'})
    }
    const password = await bcrypt.hash(plaintext, 10);
    try{
        console.log("exe")
        const newTeacher = await teachers.create({
            name,
            email,
            password, 
            role
        })
        res.send("Registration successfully")
    } catch(err){
        throw err;
    }
    console.log(req.body)
})

router.post('/sign_in', async (req, res) => {
    const {email, password} = req.body;
    const getTeacher = await teachers.findOne({email}).lean();
    if(!getTeacher){
        res.json({status: 'ok', msg: 'Email not found!!!'})
    }
    if(await bcrypt.compare(password, getTeacher.password)){
        const token = jwt.sign(
            {
                id: getTeacher._id,
                email: getTeacher.email
            },
            JWT_SECRET
        )
        return res.json({status: 'ok', data: token})
    }
    else{
        return res.json({status: 'ok', msg: 'Incorrect email or password!!!'})
    }
})

// classes
// router.post('/:teacher_id/add_classes', async (req, res) => {
//     const {teacher_id} = req.params;
//     const getTeacher = await teachers.find({'_id': teacher_id}).lean();
//     console.log(getTeacher)
//     const {_id, name: teacher_name, email: teacher_email} = getTeacher[0]

//     const {
//         class_name,
//         class_instructions,
//         // students,
//     } = req.body;

//     const newClass = {
//         class_name,
//         class_instructions
//     }

//     const teacher_info = {
//         teacher_name,
//         teacher_email
//     }

//     try{
//         const addClass = await classes.create({
//             class_name,
//             class_instructions,
//             // students,
//             teacher_info
//         })
//         res.send("New class added successfully!!!")
//     } catch(err){
//         throw err;
//     }
// })

router.post('/:teacher_email/add_class', async(req, res) => {
    const {teacher_email} = req.params;
    const getTeacher = await teachers.find({'email': teacher_email}).lean()
    console.log(getTeacher[0])
    console.log(teacher_email)
    const name = getTeacher[0].name
    const teacherInfo = {
        name,
        teacher_email
    }
    const {
        class_name,
        class_instructions
    } = req.body;

    try{
        const addClass = await classes.create({
            class_name,
            class_instructions,
            teacherInfo
        })
        res.send("New class added");
    } catch(err){
        throw err;
    }

})


// add new instruction in the class

router.post('/:teacher_id/:class_id/add_new_instructions', async (req, res) => {
    const {
        teacher_id,
        class_id
    } = req.params;

    const {
        instruction_title
    } = req.body;

    const newInstruction = {
        instruction_title
    }

    try{
        let addInstruction = await classes.updateOne(
            {'_id': class_id},
            {
                "$push":
                {
                    "class_instructions": newInstruction
                }
            }
        )
        res.send("New instruction added successfully!!!")
    }catch(err){
        throw err;
    }

})


// list of all students who enrolled for classes
router.get('/:teacher_email/:class_id/student_list', async (req, res) => {
    const {teacher_email, class_id} = req.params;

    const getCourse = await classes.find({'_id': class_id, 'teacher_email': teacher_email}).lean();

    res.send(getCourse[0].students)
})


// get all classes from "classes" collection where with teacher email id
router.get('/:teacher_email/class_lists', async (req, res) => {
    const {teacher_email} = req.params;

    const getClasses = await classes.find({'teacher_email': teacher_email}).lean();

    res.send(getClasses[0])
})


// delete a student from particular class
router.delete('/:teacher_email/:class_id/:student_id/remove_from_course', async (req, res) => {
    const {
        teacher_email,
        class_id,
        student_id
    } = req.params;

    try{
        const removeStudent = await classes.updateOne(
            {'_id': class_id, 'teacher_email': teacher_email},
            {
                "$pull":{
                    "students": {
                        "student_id": student_id
                    }
                }                
            }
        )
        console.log("Updated");
        res.send(`${student_id} removed from list`);
    }catch(err){
        throw err;
    }
});

module.exports = router;