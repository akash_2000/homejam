const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();


require('dotenv').config();

const teacherRoutes = require('./Routes/teachers');
const studentRoutes = require('./Routes/students');

const PORT = 5000;

app.use(bodyParser.json())

app.use('/teachers', teacherRoutes);
app.use('/students', studentRoutes);


mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.fwn1v.mongodb.net/class_app?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => {
    console.log("Connected")
    console.log(mongoose.connection.readyState)
});
app.listen(PORT, () => {
    console.log("Server is running on port 5000");
})