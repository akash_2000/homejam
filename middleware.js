const jwt = require('jsonwebtoken');
const JWT_SECRET = "kakhshj!@#%$#&*hjfjhaj659uhdsuuhsdjkehdhu";

const isUserAuthenticated = (req, res, next) => {
    const authHeader = req.headers.authorization
  
    if (!authHeader) {
      return res.status(403).json({
        status: 403,
        message: 'FORBIDDEN'
      })
    } else {
      const token = getBearerToken(authHeader)
  
      if (token) {
        return verifyTokenAndGetUID(token)
          .then((userId) => {
          // ------------------------------------
          // HI I'M THE UPDATED CODE BLOCK, LOOK AT ME
          // ------------------------------------
            res.locals.auth = {
              userId
            }
            console.log("You have the permission");
            next()
          })
          .catch((err) => {
            logger.logError(err)
  
            return res.status(401).json({
              status: 401,
              message: 'UNAUTHORIZED'
            })
          })
      } else {
        return res.status(403).json({
          status: 403,
          message: 'FORBIDDEN'
        })
      }
    }
  }

  const isAuth = (req, res, next) => {
    const authHeader = req.headers.authorization
  
    if (!authHeader) {
      return res.status(403).json({
        status: 403,
        message: 'FORBIDDEN'
      })
    }
    else{

        const token = authHeader.split(' ')[1]
        
        try{
            const user = jwt.verify(token, JWT_SECRET);
            if(user){
                next();
            }
            else{
                return res.status(403).json({
                    status: 403,
                    message: 'FORBIDDEN'
                  })
            }
        }catch(err){
            throw err;
        }
    }
  }
module.exports = {isUserAuthenticated, isAuth}